paw-arch-dark
=============

A plymouth theme for Arch Linux, based on plymouth-theme-paw-arch

===========
Themes used
===========

:plymouth-theme-arch-logo-new: https://aur.archlinux.org/packages/plymouth-theme-paw-arch/

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
